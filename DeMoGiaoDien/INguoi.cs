﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeMoGiaoDien
{
    interface INguoi
    {
        void nhap();
        void hienThi();

        int tuoi
        {
            get;
        }
    }
}
