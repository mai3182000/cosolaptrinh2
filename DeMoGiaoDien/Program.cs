﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeMoGiaoDien
{
    class Program
    {
        static void Main(string[] args)
        {

            INguoi sv1 = new SinhVien();

            // Khai bao doi tuong thong qua interface thi phai chi ro lop thuc thi la lop nao
            // INguoi sv2 = new INguoi();

            // Demo try, cat
            int a, b;
            try
            {
                Console.WriteLine("hay nhap a ");
                a = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Hay nhap b ");
                b = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Thuong cua a/b la: {}", a / b);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            finally
            {
                Console.ReadLine();
            }
           
        }
    }
}
