﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chuong3_part2_bai2.HinhHoc
{
    class HinhChuNhat: HinhHoc
    {
        private  int chieuRong, chieuDai;
        public override double chuVi()
        {
            return 2 * (chieuRong + chieuDai);
        } 
        public override double dienTich()
        {
            return chieuDai * chieuRong;
        }

        public override void nhap()
        {
            Console.WriteLine("hay nhap vao chieu rong hinh chu nhat: ");
            chieuRong = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Hay nhap vao chieu Dai cua hinh chu nhat: ");
            chieuDai = Convert.ToInt32(Console.ReadLine());
        }


    }
}
